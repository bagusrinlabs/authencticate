<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use App\Core\Manager\RedisManager;
use Validator;

class AuthController extends Controller
{   


    private $request;
    
    public function __construct(Request $request) {
        $this->request = $request;
    }

    protected function jwt(User $user) {
        
        $payload = [
            'iss' => "pawoon-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*env('JWT_TTL') // Expiration time
        ];
        
        return JWT::encode($payload, env('JWT_SECRET'));
    } 

    public function login(User $user) {

        try {
        
            $this->validate($this->request, [
                'email'     => 'required|email',
                'password'  => 'required'
            ]);

            $user = User::where('email', $this->request->input('email'))->first();
            if (!$user) {
                
                return response()->json([
                    'success' => false,
                    'message' => 'Email does not exist'
                ], 400);
            }

            
            if (Hash::check($this->request->input('password'), $user->password)) {

                $token = $this->jwt($user);

                $redis = new RedisManager();
                $addKey = $redis->generateAdditionalKey([]);
                $redis->set($token.$addKey, $user, 60*env('JWT_TTL'));

                return response()->json([
                    'success' => true,
                    'token' => $this->jwt($user)
                ], 200);
            }

            return response()->json([
                'success' => false,
                'message' => 'Invalid password'
            ], 400);

        } catch (\Exception $e) {

            return response()->json(['success' => false, 'message' => 'error'], 500);

        }
    }

    public function refresh() {

        try {

            $token = $this->request->get('token');
            $payload = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

            return response()->json($payload,200);

        } catch (\Firebase\JWT\ExpiredException $e) {
            $payload = "expired";

            JWT::$leeway = 720000;
            $decoded = (array) JWT::decode($token, env('JWT_SECRET'), ['HS256']);
            
            // TODO: test if token is blacklisted
            $decoded['iat'] = time();
            $decoded['exp'] = time() + 60;

            $refreshtoken = JWT::encode($decoded, env('JWT_SECRET'));

            $redis = new RedisManager();

            $payload = JWT::decode($refreshtoken, env('JWT_SECRET'), ['HS256']);
            $data = User::where('id', $payload->sub)->first();

            $addKey = $redis->generateAdditionalKey([]);
            $redis->set($refreshtoken.$addKey, $data, 60*env('JWT_TTL'));
            
            return response()->json([
                "success" => true,
                "token" => $refreshtoken
            ]);
        
        } catch (\Exception $e) {

            return response()->json(['success' => false, 'message' => 'error'], 500);

        }
        
    }

    public function logout() {

        try {

            $token = $this->request->get('token');
            JWT::$leeway = 720000;
            $decoded = (array) JWT::decode($token, env('JWT_SECRET'), ['HS256']);
            $decoded['exp'] = time() + 60;
            return response()->json(JWT::encode($decoded, env('JWT_SECRET')),200);

        } catch (\Exception $e) {

            return response()->json(['success' => false, 'message' => 'error'], 500);

        }
    }

    public function me(User $user) {

        try {

            $redis = new RedisManager();
            $searchParam = [];

            $key = $this->request->get('token');

            $cache = ["cache" => true];
            $data = $redis->get($key, $searchParam);
            
            if (empty($data)) {
                $cache = ["cache" => false];
                $payload = JWT::decode($key, env('JWT_SECRET'), ['HS256']);

                $data = User::where('id', $payload->sub)->first();

                $addKey = $redis->generateAdditionalKey($searchParam);
                $redis->set($key.$addKey, $data, 60*env('JWT_TTL'));
            }

            return response()->json(["success" => true, "data" => $data]);

        } catch (\Exception $e) {

            return response()->json(['success' => false, 'message' => 'error'], 500);

        }
    }
}
