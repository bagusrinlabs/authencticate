<?php
namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {   
        // $header = $request->header('Authorization');
        // $matches = [];

        // if (! preg_match('/bearer\s(\S+)/', $header, $matches)) {
        //     return response()->json([
        //         'success' => false,
        //         'code' => 'inv',
        //         'message' => 'Missing authorization header'
        //     ], 401);  
        // }

        // //$token = $matches[1];
        // //$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjIsImlhdCI6MTYzMDgzODY2OCwiZXhwIjoxNjMwODM4NzI4fQ.QVmH4dzGhgFxG9w2XQvAlDNwwsCWh-u1ezaeebIABZM";
        // //$token = $request->get('token');
        // //return response()->json($token);

        // $authHeader = $_SERVER['HTTP_AUTHORIZATION'];

        // $token = explode(" ", $authHeader)[1];
        // //return response()->json($token);

        $token = $request->get('token');
        if(!$token) {
            return response()->json([
                'success' => false,
                'code' => 'inv',
                'message' => 'Invalid token'
            ], 401);
        }

        try {

            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        
        } catch(ExpiredException $e) {
            
            return response()->json([
                'success' => false,
                'code' => 'exp',
                'message' => 'Token is expired'
            ], 400);
        
        } catch(Exception $e) {
            return response()->json([
                'success' => false,
                'code' => 'inv',
                'error' => 'Invalid token'
            ], 401);
        }

        //$user = User::find($credentials->sub);
        // Now let's put the user in the request class so that you can grab it from there
        //$request->auth = $user;
        //$request->token = $token;
        return $next($request);
    }
}