<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[	
        		'uuid' => Str::uuid(),
            	'email' => 'bagusrin.xyz@gmail.com',
            	'password' => Hash::make('superpawoon'),
            	'name' => 'Bagus Rinaldhi'
        	],
        	[	
        		'uuid' => Str::uuid(),
        		'email' => 'alula.nayya@gmail.com',
            	'password' => Hash::make('superpawoon'),
            	'name' => 'Alula Nayya'
        	]
        ]);
    }
}
